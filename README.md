# Sample K8s deployment scaler with RBAC
This repo contains some sample code for scaling a deployment using another deployment & kubectl

## Contents
- `Dockerfile`: Dockerfile for the custom-autoscaler image
- `scale.py`: The scaler script (gets copied into the docker image)
- `manifests/app.yaml`: Sample application which will be scaled
- `manifests/custom-autoscaler.yaml`: custom-autoscaler deployment with service account & role
