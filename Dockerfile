FROM python:alpine
RUN apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing kubectl
COPY scale.py /
CMD ["python", "scale.py"]
