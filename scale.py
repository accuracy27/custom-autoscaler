import os, time
  
ca_cert = "/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
with open("/var/run/secrets/kubernetes.io/serviceaccount/token", "r") as f:
    bearer_token = f.read()

# We presume the deployment is already running. 
deployment_name = "deployment.apps/helloweb"

# Going up
n = 0
while n < 5:
    n += 1
    os.system('kubectl --token={} --certificate-authority={} scale {} --replicas={}'.format(bearer_token, ca_cert, deployment_name, n))
    time.sleep(20)

# Going down
while n > 0:
    n -= 1
    os.system('kubectl --token={} --certificate-authority={} scale {} --replicas={}'.format(bearer_token, ca_cert, deployment_name, n))
    time.sleep(20)
